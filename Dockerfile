FROM php:7.1-apache

RUN apt-get update \
  && apt-get install -y curl php7.1-xml php7.1-mbstring php7.1-mysql php7.1-json php7.1-curl php7.1-cli php7.1-common php7.1-mcrypt php7.1-gd libapache2-mod-php7.1 php7.1-zip \
  && curl -sS https://getcomposer.org/installer | php \
  && mv composer.phar /usr/bin/composer \
  && apt-get clean \
  && rm -rf /var/cache/apt/archives

COPY laravel-code/ /var/www/html/